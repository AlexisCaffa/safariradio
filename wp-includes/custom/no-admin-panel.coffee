"use strict"
$ = jQuery

$.urlParam = (name)->
    results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href)
    return results[1] || 0


redirect_advanced_ads_menu = ->
    for a in $('a.toplevel_page_advanced-ads')
        a.href = '/wp-admin/edit.php?post_type=advanced_ads'
        $(a).find('.wp-menu-name').html 'Anuncios'
    return
replace_words_advanced_ads = ->
    for el in $('#advanced-ad-type').find '.description'
        txt = $(el).html()
        txt = txt.replace /de WordPress/gi, ''
        txt = txt.replace /WordPress/gi, ''
        $(el).html(txt)
    return

remove_wp_statistics_options = ->
    #hide last [-X..] li menu elements
    for li in $('#toplevel_page_wps_overview_page').find('li')[-5..]
        $(li).hide()
    ###
    # Elimina las «a» de la tabla, para ocultar IPs
    tabla_resumen = $('#summary-stats')
    if tabla_resumen.length
        for th in tabla_resumen.find('.th-center')
            a = $(th).find 'a'
            if a.length
                a.find('span').appendTo($(th))
                a.remove()
    ###
    return

remove_email_users_donation_side = ->
    if $.urlParam('page') is "email-users%2Femail-users.php"
        $('.postbox-container.side').remove()
        # remove wordpress word
        for p in $('.postbox-container p')
            if $(p).text().indexOf('wordpress') >= 0 or \
                $(p).text().indexOf('WordPress') >= 0
                 text = $(p).text().replace /wordpress/gi, ''
                 $(p).text(text)
    return

$(document).ready ->
    redirect_advanced_ads_menu()
    replace_words_advanced_ads()
    remove_wp_statistics_options()
    remove_email_users_donation_side()