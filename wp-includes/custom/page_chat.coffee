$ = jQuery

get_src_mibbit = (server, channel, title, theme)->
    "http://widget.mibbit.com/?server=#{server}&nick=&showJoinsParts=false&chatOutputShowTimes=false&parseSmilies=true&noServerTab=true&langage=es&customprompt=#{encodeURIComponent title}&Userlistwidth=140&settings=#{theme}&channel=%23#{channel}"
get_src_kiwi = (server, channel, theme='cli')->
    "https://kiwiirc.com/client/#{server}/?&theme=#{theme}##{channel}"

CHATROOM_SRC =
    argentina: "http://dalechatea.me/mibbit.html?settings=&server=irc.mibbit.net&channel=%23dalechat_libre&nick="
    rosarinos: get_src_mibbit 'irc.rizon.net', 'rosarinos', 'Rosarinos', '22041f31cac0adf2d1959cdc21aed853'
    vale_todo: get_src_mibbit 'irc.rizon.net', 'valetodo', 'Vale Todo', '3627414e3771f9250fce659187969141'
    gay_rosario: "http://dalechatea.me/chat-hot.php?server=irc.mibbit.net%3A%2B6697&amp;channel=%23dalechat_hot&amp;nick=&amp;userListShowIcons=true"
#    gay_rosario: get_src_mibbit 'irc.rizon.net', 'gayrosario', 'Gay Rosario', 'e04a79313c47f155f346aa1b25615a06'

IFRAME_SELECTOR = $('#iframe_chat')

init_chat = ->
    hash = get_hash()
    if CHATROOM_SRC[hash]
        set_iframe_src CHATROOM_SRC[hash]
    else
        set_hash 'argentina'
    switch hash
        when 'gay_rosario' then $('.over-chat').addClass 'flat'
        when 'argentina' then $('.over-chat').addClass 'flat'
        else $('.over-chat').removeClass 'flat'


get_hash = -> window.location.hash.replace '#', ''

set_hash = (hash)->
    if window.history.pushState then window.history.pushState null, null, '#'+hash
    else window.location.hash = '#'+hash
    set_iframe_src CHATROOM_SRC[hash]

set_iframe_src = (src)->
    IFRAME_SELECTOR.attr 'src', 'nn.com'
    IFRAME_SELECTOR.attr 'src', src


start_playing = ->
    $(document).ready ->
        $('#over_chat').append  """
            <div class="box">
                <div id="uniquePlayer-5" class="webPlayer light audioPlayer">
                    <div id="uniqueContainer-5" class="videoPlayer"></div>
                    <div style="display:none;" class="playerData">
                        {
                        "name": "SAFARI RADIO - Rosario",
                        "size": {
                        "width": "400px"
                        },
                        "media": {
                        "mp3": "http://server.ohradio.com.ar:9304/;",
                        "aac": "http://server.ohradio.com.ar:9304/;"
                        }
                        }
                    </div>
                </div>
            </div>
            """
        setTimeout ->
                player = $('.webPlayer').first()
                player.videoPlayer()
                setTimeout ->
                    player.find('.videoPlayer').jPlayer('play')
                , 500
            , 500

###
    Inicio
###
window.addEventListener 'hashchange', ->
    init_chat()

init_chat()
start_playing()