"use strict"
$ = jQuery

BTN_RADIO_VIVO = 'Escuchar »'
BTN_DESCARGAR_APP_RADIO = 'Descargar »'
BTN_DESCARGAR_APP_CHAT = 'Descargar »'
URL_RADIO_VIVO = '/radio-en-vivo'


open_radio = (url=URL_RADIO_VIVO)->
    is_mobile = /(mobile)/i.test(navigator.userAgent)
    if is_mobile
        win = window.open(url, '_blank')
        win.focus()
    else
        window.open(url, 'SAFARI RADIO - EN VIVO', "width=432,height=400,scrollbars=no,menubar=no,status=no,toolbar=no")


custom_a_open = (a_element)->
    a_element.href = 'javascript:void(0)'
    a_element.target = '_self'
    a_element.onclick = (e)->
        e.preventDefault()
        open_radio()

personalize_fp_one = ->
    fp = $('.span4.fp-one')
    custom_a_open a for a in fp.find 'a'
    fp.find('.btn.btn-primary').text BTN_RADIO_VIVO

personalize_fp_two = ->
    fp = $('.span4.fp-two')
    for a in fp.find 'a'
        a.href = 'https://play.google.com/store/apps/details?id=ar.com.safariradio.chat'
        a.target = '_blank'
    fp.find('.btn.btn-primary').text BTN_DESCARGAR_APP_RADIO

personalize_fp_three = ->
    fp = $('.span4.fp-three')
    for a in fp.find 'a'
        a.href = 'https://play.google.com/store/apps/details?id=ar.com.safariradio'
        a.target = '_blank'
    fp.find('.btn.btn-primary').text BTN_DESCARGAR_APP_CHAT

personalize_menu_radio_en_vivo = ->
    a_elements = $('.menu-item-type-custom').find 'a'
    custom_a_open a for a in a_elements when a.href.indexOf('radio-en-vivo')>=0

adapt_menu_ingresar = ->
    logged = $('.logout-link').length
    li_elements = $('.menu-item-81').find 'li'
    if logged
        $(li_elements[0]).hide()
        $(li_elements[1]).hide()
        $(li_elements[3]).hide() #footer
        $(li_elements[4]).hide() #footer
        $('#social_login').hide() #hide widget
    else
        $(li_elements[2]).hide()
        $(li_elements[5]).hide() #footer

personalize_feos_buttons = ->
    $('#user-submit').addClass 'btn btn-primary '
    $('#bbp_topic_submit').addClass 'btn btn-primary '

###
    Start!
###
personalize_fp_one()
personalize_fp_two()
personalize_fp_three()
personalize_menu_radio_en_vivo()
adapt_menu_ingresar()
personalize_feos_buttons()