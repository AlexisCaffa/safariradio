<?php

include("class.mysqlidb.php");

class DB
{
    public function save($SERVER, $POST, $response) {

        $db = new MysqliDb ('mysql.hostinger.com.ar', 'u442132003_pl', 'G|c`Jk`1DTk', 'u442132003_pl');

        $data = Array (

            'name' => $this->clean_string($POST['name']),

            'email' => $this->contact_resolve($POST['contact'], $POST['type'], "email"),
            'phone' => $this->contact_resolve($POST['contact'], $POST['type'], "phone"),
            'type' => $this->clean_string($POST['type']),

            'message' => $POST['message'],

            'created_time' => $db->now(),
            'success' => $response['success'],

            'comments' => $this->contact_resolve($POST['contact'], $POST['type'], "comments"),

            'ip_from' => $this->get_ip($SERVER)
        );


        $id = $db->insert ('contacts', $data);

        if ($id) {

            $results = Array(

                'data' => $id,
                'success' => true

            );
            return $results;
        }
        else {

            $results = Array(

                'data' => $db->getLastError(),
                'success' => false

            );
            return $results;
        }

    }

    protected function clean_string($string)
    {
        if (isset($string)) {

            $bad = array("content-type", "bcc:", "to:", "cc:", "href");

            return str_replace($bad, "", $string);

        } else {

            return null;
        }
    }

    protected function contact_resolve($contact, $type, $should_be)
    {
        if ($this->clean_string($type) == $should_be) {

            return $this->clean_string($contact);

        } elseif ( $should_be == "comments"
                   and $this->clean_string($type) != "phone"
                   and $this->clean_string($type) != "email") {

            return $this->clean_string($contact);

        } else {

            return null;
        }
    }

    protected function get_ip($SERVER)
    {
        foreach (array('HTTP_CLIENT_IP', 'HTTP_X_FORWARDED_FOR', 'HTTP_X_FORWARDED', 'HTTP_X_CLUSTER_CLIENT_IP', 'HTTP_FORWARDED_FOR', 'HTTP_FORWARDED', 'REMOTE_ADDR') as $key)
        {
            if (array_key_exists($key, $SERVER) === true)
            {
                foreach (array_map('trim', explode(',', $SERVER[$key])) as $ip)
                {
                    if (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE) !== false)
                    {
                        return $ip;
                    }
                }
            }
        }
    }


}
