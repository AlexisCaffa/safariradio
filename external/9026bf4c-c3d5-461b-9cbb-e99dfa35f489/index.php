<?php

include("class.cors.php");
include("class.requestmethod.php");
include("class.db.php");

// Habilitamos CORS
$cors = new CORS();
$cors->enable_cors();


$method = new RequestMethod();

if ($method->is('GET')) {

  header('Content-Type: application/json');

  $data = file_get_contents("package-config.json");

  die($data);

} else {

  $method->redirect('http://theuselessweb.com', 404);

}