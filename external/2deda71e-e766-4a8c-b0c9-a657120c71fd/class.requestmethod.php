<?php

class RequestMethod
{
  public function is($search_method)
  {
    $request_method = $_SERVER['REQUEST_METHOD'];
    if (strtoupper($search_method) == $request_method) {
      return true;
    } else {
      return false;
    }
  }

  public function is_post()
  {

    $method = $_SERVER['REQUEST_METHOD'];

    switch ($method) {

      case 'PUT':
      case 'GET':
      case 'DELETE':
        $this->redirect("");
        $return = false;
        break;
      case 'POST':
        $return = true;
        break;
      default:
        $return = false;
        break;
    }

    return $return;
  }

  function redirect($url, $statusCode = 303)
  {
    header('Location: ' . $url, true, $statusCode);
    die();
  }

}
