<?php
/**
*
* This program is a free software; you can use it and/or modify it under the terms of the GNU
* General Public License as published by the Free Software Foundation; either version 2 of the License,
* or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
* even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
*
* You should have received a copy of the GNU General Public License along with this program; if not, write
* to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
*
* @package   	Customizr
* @subpackage 	functions
* @since     	1.0
* @author    	Nicolas GUILLAUME <nicolas@presscustomizr.com>
* @copyright 	Copyright (c) 2013-2015, Nicolas GUILLAUME
* @link      	http://presscustomizr.com/customizr
* @license   	http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
*/


/**
* This is where Customizr starts. This file defines and loads the theme's components :
* => Constants : CUSTOMIZR_VER, TC_BASE, TC_BASE_CHILD, TC_BASE_URL, TC_BASE_URL_CHILD, THEMENAME, TC_WEBSITE
* => Default filtered values : images sizes, skins, featured pages, social networks, widgets, post list layout
* => Text Domain
* => Theme supports : editor style, automatic-feed-links, post formats, navigation menu, post-thumbnails, retina support
* => Plugins compatibility : JetPack, bbPress, qTranslate, WooCommerce and more to come
* => Default filtered options for the customizer
* => Customizr theme's hooks API : front end components are rendered with action and filter hooks
*
* The method TC__::tc__() loads the php files and instantiates all theme's classes.
* All classes files (except the class__.php file which loads the other) are named with the following convention : class-[group]-[class_name].php
*
* The theme is entirely built on an extensible filter and action hooks API, which makes customizations easy and safe, without ever needing to modify the core structure.
* Customizr's code acts like a collection of plugins that can be enabled, disabled or extended.
*
* If you're not familiar with the WordPress hooks concept, you might want to read those guides :
* http://docs.presscustomizr.com/article/26-wordpress-actions-filters-and-hooks-a-guide-for-non-developers
* https://codex.wordpress.org/Plugin_API
*/


//Fire Customizr
require_once( get_template_directory() . '/inc/init.php' );

/**
* THE BEST AND SAFEST WAY TO EXTEND THE CUSTOMIZR THEME WITH YOUR OWN CUSTOM CODE IS TO CREATE A CHILD THEME.
* You can add code here but it will be lost on upgrade. If you use a child theme, you are safe!
*
* Don't know what a child theme is ? Then you really want to spend 5 minutes learning how to use child themes in WordPress, you won't regret it :) !
* https://codex.wordpress.org/Child_Themes
*
* More informations about how to create a child theme with Customizr : http://docs.presscustomizr.com/article/24-creating-a-child-theme-for-customizr/
* A good starting point to customize the Customizr theme : http://docs.presscustomizr.com/article/35-how-to-customize-the-customizr-wordpress-theme/
*/

/**
 * Hide admin panel items to no-admin users.
 *
 * ContactForm7, Herramientas, Ajustes, Wp-Logo, Ayuda, Opciones de pantalla, Wordpress footer y detallitos
 */
if (!(current_user_can('administrator') || current_user_can('programator'))) {

  function remove_menus() {
    remove_menu_page( 'wpcf7' );
    remove_menu_page( 'tools.php' );
    remove_menu_page( 'options-general.php' );
  }
  add_action('admin_menu', 'remove_menus');

  function remove_wp_logo() {
    global $wp_admin_bar;
    $wp_admin_bar->remove_menu('wp-logo');
  }
  add_action('wp_before_admin_bar_render', 'remove_wp_logo', 0);

  function remove_help_tabs() {
    $screen = get_current_screen();
    $screen->remove_help_tabs();
  }
  add_action('admin_head', 'remove_help_tabs');


  function admin_custom_scripts() {
    wp_enqueue_style('my-admin-style', '/wp-includes/custom/no-admin-panel.css');
    wp_enqueue_script('my-admin-script', '/wp-includes/custom/no-admin-panel.min.js');
  }
  add_action('admin_enqueue_scripts', 'admin_custom_scripts');
}


/**
 * Remove capabilities from editors.
 *
 * Call the function when your plugin/theme is activated.
 */
function set_editor_capabilities() {

  // Get the role object.
  $editor = get_role( 'editor' );
  // A list of capabilities to remove from editors.
  $caps_to_remove = array(
    'delete_others_pages',
    'elete_pages',
    'delete_private_pages',
    'delete_published_pages',
    'edit_others_pages',
    'edit_pages',
    'edit_private_pages',
    'edit_published_pages',
    'publish_pages',
    'read_private_pages',
  );
  foreach ( $caps_to_remove as $cap ) {
    // Remove the capability.
    $editor->remove_cap( $cap );
  }
  // A list of capabilities to add to editors.
  $caps_to_add = array(
    'list_users',
  );
  foreach ( $caps_to_add as $cap ) {
    // Add the capability.
    $editor->add_cap( $cap );
  }
}
add_action( 'init', 'set_editor_capabilities' );



/**
 * Create programator role.
 *
 * Allow to add and edit pages
 */
function add_programator_role() {
  add_role(
    'programator',
    __( 'Programador' ),
    array(
      'moderate_comments' => true,  // true allows this capability
      'delete_posts'      => false, // Use false to explicitly deny
    )
  );
}
// Only executed once
//add_action( 'init', 'add_programator_role' );

function set_programator_capabilities() {

  // Get the role object.
  $programator = get_role( 'programator' );
  // A list of capabilities to add to programators.
  $caps_to_add = array(
    'read',
    'activate_plugins',
    'edit_theme_options',
    'update_plugins',
    'upload_plugins',
    'install_plugins',
    'delete_plugins',
    'edit_plugins',
    'import',
    'export',
    'edit_files',
    'customize',
    'update_core',
    'edit_pages',
    'edit_others_pages',
    'edit_private_pages',
    'edit_published_pages',
    'publish_pages',
    'upload_files',
    'edit_others_posts',
    'edit_posts',
    'edit_private_posts',
    'edit_published_posts',
    'publish_posts',
    'manage_categories',
    'manage_links',
    'manage_options',
  );
  foreach ( $caps_to_add as $cap ) {
    // Add the capability.
    $programator->add_cap( $cap );
  }
}
add_action( 'init', 'set_programator_capabilities' );



/**
 * Hide admin bar for suscriptor user.
 */
function remove_admin_bar() {
  if ( !current_user_can('edit_posts') ) {
    show_admin_bar(false);
  }
}
add_action('after_setup_theme', 'remove_admin_bar');